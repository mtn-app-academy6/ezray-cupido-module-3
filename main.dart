import 'package:flutter/material.dart';
import 'package:flutter_application_1/Login.dart';
import 'package:flutter_application_1/UserProfileEdit.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(scaffoldBackgroundColor: Colors.white70),
        initialRoute: '/',
        routes: {
          '/': (context) => Login(),
          '/screen2': (context) => Registration(),
          '/screen3': (context) => Dashboard(),
          '/screen4': (context) => FeatureScreen1(),
          '/screen5': (context) => FeatureScreen2(),
          '/screen6': (context) => UserProfileEdit(),
        });
  }

}
