import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter_application_1/UserProfileEdit.dart';

class UserProfileEdit extends StatelessWidget {
  const UserProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: const Text("UserProfileEdit"),
        ),
        body: ElevatedButton(
          onPressed: () => {
            Navigator.pushNamed(context, '/screen3'),
          },
          child: const Text("Welcome to Your Profile"),
        ));
  }
}


class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.blueGrey, title: const Text("Feature 1")),
        body: ElevatedButton(
          onPressed: () => {
            Navigator.pushNamed(context, '/screen5'),
          },
          child: const Text("This is feature 1"),
        ));
  }
}


class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: const Text("Features 2"),
        ),
        body: ElevatedButton(
          onPressed: () => {
            Navigator.pushNamed(context, '/screen6'),
          },
          child: const Text("This feature 2"),
        ));
  }
}


